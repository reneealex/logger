﻿using System.Configuration;

namespace Logger
{
    public sealed class Constant
    {
        public static string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
        public static string LogFileDirectory = ConfigurationManager.AppSettings["LogFileDirectory"]; 
    }
}
