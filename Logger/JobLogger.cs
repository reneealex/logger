﻿using System;
using System.IO;

namespace Logger
{
    public partial class JobLogger
    {
        private static bool _logToFile;
        private static bool _logToConsole;
        private static bool _logMessage;
        private static bool _logWarning;
        private static bool _logError;
        private static bool _logToDatabase;        
        private bool _initialized;

        public JobLogger(bool logToFile, bool logToConsole, bool logToDatabase, bool logMessage, bool logWarning, bool logError)
        {
            _logError = logError;
            _logMessage = logMessage;
            _logWarning = logWarning;
            _logToDatabase = logToDatabase;
            _logToFile = logToFile;
            _logToConsole = logToConsole;            
        }

        public static void LogMessage(string message, bool flag, bool warning, bool error)
        {
            var jobLoggerService = new JobLoggerService(_logToFile, _logToConsole, _logMessage, _logWarning, _logError, _logToDatabase);
            var connection = new Connection();
            int typeLog = 0;
            string textFile = string.Empty;
            string dateNow = DateTime.Now.ToShortDateString().Replace('/','-');

            if (message == null || message.Length == 0)
            {
                return;
            }

            message.Trim();
            jobLoggerService.ValidateExceptionFile(flag, warning, error);
            typeLog = jobLoggerService.EvaluateType(flag, warning, error);
            var resultConnection = connection.ExecuteQuery(message, typeLog);

            if (resultConnection)
            {
                textFile = jobLoggerService.ReturnContents(textFile, dateNow, message, flag, warning, error);
                File.WriteAllText(Constant.LogFileDirectory + "LogFile" + dateNow + ".txt", textFile);
                jobLoggerService.GetColorConsole(flag, warning, error);
                Console.WriteLine(dateNow + message);
            }
            else
            {
                Console.WriteLine(Message.MessageError);
            }
        }  
        
        public static void Main()
        {
            _logToConsole = true;
            _logError = true;
            LogMessage("Demo de prueba test 12345", false, false, true);
        }
    }
}
