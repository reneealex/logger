﻿using System;
using System.IO;

namespace Logger
{
    public class JobLoggerService
    {
        private static bool _logToFile;
        private static bool _logToConsole;
        private static bool _logMessage;
        private static bool _logWarning;
        private static bool _logError;
        private static bool _logToDatabase;

        public JobLoggerService(bool logToFile, bool logToConsole, bool logToDatabase, bool logMessage, bool logWarning, bool logError)
        {
            _logError = logError;
            _logMessage = logMessage;
            _logWarning = logWarning;
            _logToDatabase = logToDatabase;
            _logToFile = logToFile;
            _logToConsole = logToConsole;
        }

        public int EvaluateType(bool flag, bool warning, bool error)
        {
            if (flag && _logMessage)
            {
                return 1;
            }
            if (error && _logError)
            {
                return 2;
            }
            if (warning && _logWarning)
            {
                return 3;
            }
            return 0;
        }

        public string ReturnContents(string textFile, string dateNow, string message, bool flag, bool warning, bool error)
        {
            if (File.Exists(Constant.LogFileDirectory + "LogFile" + dateNow + ".txt"))
            {
                textFile = File.ReadAllText(Constant.LogFileDirectory + "LogFile" + dateNow + ".txt");
            }

            if (error || warning || flag)
            {
                return string.Concat(textFile, " ", dateNow, " ", message);
            } 

            return textFile;
        }

        public void GetColorConsole(bool flag, bool warning, bool error)
        {
            if (error && _logError)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else if (warning && _logWarning)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
            }
            else if (flag && _logMessage)
            {
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public void ValidateExceptionFile(bool flag, bool warning, bool error)
        {
            if (!_logToConsole && !_logToFile && !_logToDatabase)
            {
                throw new Exception(Message.InvalidConfiguration);
            }
            if ((!_logError && !_logMessage && !_logWarning) || (!flag && !warning && !error))
            {
                throw new Exception(Message.ErrorWarningMessage);
            }
        }
    }
}
