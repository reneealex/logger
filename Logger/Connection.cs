﻿using System;
using System.Data.SqlClient;

namespace Logger
{
    public class Connection
    {
        public bool ExecuteQuery(string message, int typeLog)
        {
            using (var connection = new SqlConnection(Constant.ConnectionString))
            {
                try
                { 
                    connection.Open();
                    SqlCommand command = new SqlCommand("Insert into Log Values('" + message + "', " + typeLog.ToString() + ")", connection);
                    command.ExecuteNonQuery();
                    connection.Close();
                    return true;
                }
                catch (SqlException sqlEx)
                {
                    Console.WriteLine(sqlEx.Message);
                    return false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }
            }
        }
    }
}
