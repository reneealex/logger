﻿namespace Logger
{
    public static class Message
    {
        public static string InvalidConfiguration = "Invalid configuration";
        public static string ErrorWarningMessage = "Error or Warning or Message must be specified";
        public static string MessageError = "There is an error";
    }
}
