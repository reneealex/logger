﻿using Logger;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestLogger
{
    [TestClass]
    public class UnitTest1
    {        
        [TestMethod]
        public void TestMethod1()
        {
            var jobLoggerService = new JobLoggerService(false, false, false, true, false, false);
            var result1 = jobLoggerService.EvaluateType(true, false, false);
            Assert.AreEqual(1, result1);
        }

        [TestMethod]
        public void TestMethod2()
        {
            var jobLoggerService = new JobLoggerService(false, false, false, false, false, true);
            var result2 = jobLoggerService.EvaluateType(false, false, true);
            Assert.AreEqual(2, result2);
        }

        [TestMethod]
        public void TestMethod3()
        {
            var jobLoggerService = new JobLoggerService(true, true, true, true, true, true);
            var result3 = jobLoggerService.EvaluateType(false, true, false);
            Assert.AreEqual(3, result3);
        }
    }
}
